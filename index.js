import http from "http";
import dotenv from "dotenv";
import url from "url";
import { readFile } from "fs/promises";
dotenv.config();

const getNewsData = async () => {
    try {
      const data = await readFile("./newsData.json", "utf-8");
      return JSON.parse(data);
    } catch (err) {
      console.error(err);
      return [];
    }
  };

const newsServer = http.createServer(async (req, res) => {
    const parsedUrl = url.parse(req.url, true);
      const newsData = await getNewsData();

  if (parsedUrl.pathname === "/" && req.method === "GET") {
    try {
      const page = parsedUrl.query?.page ?? 1;
      const size = parsedUrl.query?.size ?? 10;
      console.log(parsedUrl.query,parsedUrl.query.page );
      const startIndex = (page - 1) * size;
      const endIndex = startIndex + size;
      const news = newsData.slice(startIndex, endIndex);
      console.log("news selected: ",news, startIndex, endIndex);
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(JSON.stringify(news));
    } catch (error) {
      console.error(error);
      res.writeHead(500, { "Content-Type": "text/plain" });
      res.end("Internal Server Error");
    }
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end("Not Found");
  }
});

newsServer.listen(process.env.PORT, process.env.HOST, () => {
  console.log(`Server is up on ${process.env.HOST}:${process.env.PORT}`);
});

